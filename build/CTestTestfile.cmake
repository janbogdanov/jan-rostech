# CMake generated Testfile for 
# Source directory: /home/jan/jan-rostech/src
# Build directory: /home/jan/jan-rostech/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs(gtest)
subdirs(my_r2d2)
subdirs(est_voice_synth)
subdirs(r2d2_navigator)
